import { createRouter, createWebHashHistory } from "vue-router";

const routes = [
  { path: "/", redirect: { name: "Home" }, },
  { path: "/home", name: "Home", component: () => import("@/views/Home/Home.vue") },
  { path: "/main", name: "Main", component: () => import("@/views/HasChat/HasChat.vue") }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;